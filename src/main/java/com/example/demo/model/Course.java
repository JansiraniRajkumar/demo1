package com.example.demo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Entity
@Table(name = "COURSES")

public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long courseId;
	
	@Column(name = "COURSE_NAME")
	private String name;
	@Column(name = "COURSE_DESC")
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE},
			mappedBy = "courses")
	@JsonIgnoreProperties("courses")
	private Set<Student> student;

	public Course() {
		super();
	}

	public Course(long courseId, String name, String description, Set<Student> student) {
		super();
		this.courseId = courseId;
		this.name = name;
		this.description = description;
		this.student = student;
	}
	
	

	public Course(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public Course(long courseId, String name, String description) {
		super();
		this.courseId = courseId;
		this.name = name;
		this.description = description;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Student> getStudent() {
		return student;
	}

	public void setStudent(Set<Student> student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", name=" + name + ", description=" + description + ", student="
				+ student + "]";
	}

	
}
