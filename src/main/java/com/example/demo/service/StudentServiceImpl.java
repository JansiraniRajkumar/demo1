package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.StudentRepository;
import com.example.demo.exception.StudentNotFoundException;
import com.example.demo.model.Course;
import com.example.demo.model.Student;


@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Student createStudent(Student student) {
		System.out.println(student.getStudentName());
		return studentRepository.save(student);
	}

	@Override
	public void removeStudent(long studentId) {
		studentRepository.deleteById(studentId);

	}

	@Override
	public Student getStudentById(long studentId) {
		Optional<Student> option = studentRepository.findById(studentId);
		Student student = null;
		if (option.isPresent()) {
			student = option.get();

		} else {
			throw new StudentNotFoundException("Student with id: " + studentId + "is not found");
		}
		return student;
	}

	@Override
	public Student getStudentByName(String studentname) {
		return studentRepository.findByStudentName(studentname);
	}

	@Override
	public Set<Course> myCourses(String studentName) {
		Student student = studentRepository.findByStudentName(studentName);

		return student.getCourses();
	}

	@Override
	public List<Student> getAllStudents() {
		List<Student> studentList = (List<Student>) studentRepository.findAll();

		if (studentList.size() > 0) {
			return studentList;
		} else {
			return new ArrayList<Student>();
		}
	}

	@Override
	public Student updateStudent(Student student) {
		Optional<Student> stud = studentRepository.findById(student.getStudentId());

		if (stud.isPresent()) {
			Student newEntity = stud.get();
			newEntity.setStudentId(student.getStudentId());
			newEntity.setStudentName(student.getStudentName());
			newEntity.setCourses(student.getCourses());
			newEntity = studentRepository.save(newEntity);
			return newEntity;
		} else {
			student = studentRepository.save(student);
			return student;

		}
	}

}
